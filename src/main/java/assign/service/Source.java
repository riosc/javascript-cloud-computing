package assign.service;

import java.io.IOException;
import java.util.logging.Logger;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Source {
	private String url = "http://eavesdrop.openstack.org/meetings/"; 	
	private static final Logger LOGGER = Logger.getLogger(Source.class.getName());

	public String getCount() throws IOException{		
		Document doc_url = Jsoup.connect(url).get();
		StringBuilder str = new StringBuilder();
		str.append("{");

		int i = 0;
		Elements correct_doc = doc_url.select("a[href]");
		for(Element correct_link: correct_doc) {
			i++; correct_link.attr("abs:href");
			if(i > 5){ 
				String pname = correct_link.attr("abs:href");
				String name = pname.substring(pname.indexOf("meetings")+9, pname.length()-1);

				Document doc_name = Jsoup.connect(url+name).get();
				Elements year_link = doc_name.select("a[href*=2]");
				int count = 0;
				for(Element year: year_link) {
					Document doc_year = Jsoup.connect(year.attr("abs:href")).get();
					Elements meeting = doc_year.select("a[href*="+name+"]");
					for(Element meet: meeting){
						if(meet.attr("abs:href").contains(".log.txt")) {
							LOGGER.info(count+"");
							count++;
						} 
					}
				}

				str.append("\"" + name + "\":" + count + ",");
			}
		}
		str.deleteCharAt(str.length()-1);
		str.append("}");

		LOGGER.info("FINISH");
		return str.toString();	
	}
}