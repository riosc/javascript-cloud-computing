package assign.resource;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/myeavesdrop")
public class ResourceApplication extends Application {
	private Set<Object> singletons = new HashSet<Object>();

	public ResourceApplication() {
		singletons.add(new Resource());
	}

	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}
}

