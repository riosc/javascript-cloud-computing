package assign.resource;

import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import assign.service.Source;

@Path("/projects")
public class Resource {
	
	Source src = new Source();
	
	@GET
	@Path("/")
	public String printMeetingYear() throws IOException {
		return src.getCount();
	}
}
